package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findByProductLineTextDescriptionContains(String segment);

    List<Product> findByQuantityInStockBetween(short low, short high);

    List<Product> findByQuantityInStockBetweenOrderByProductCode(short low, short high);

    Page<Product> findAllByQuantityInStockBetweenOrderByProductCode(short i, short i1, Pageable pageable);

    List<Product> findFirst3ByQuantityInStockBetweenOrderByProductCode(short low, short high);
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    // --end-->
}