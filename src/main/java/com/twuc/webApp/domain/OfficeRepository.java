package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OfficeRepository extends JpaRepository<Office, String> {
    List<Office> findByCity(String tokyo);
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    // --end-->
}